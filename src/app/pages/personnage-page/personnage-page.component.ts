import { Component, OnInit } from '@angular/core';
import { Personnage } from 'src/app/model/personnage';
import { PersonnageService } from 'src/app/service/personnage.service';

@Component({
  selector: 'app-personnage-page',
  templateUrl: './personnage-page.component.html',
  styleUrls: ['./personnage-page.component.css']
})
export class PersonnagePageComponent implements OnInit {

  personnages: Array<Personnage> = [];

  constructor(private personnageService: PersonnageService) { }

  ngOnInit(): void {
    this.personnageService.getPersonnage();
    this.getPersonnages();
  }
  ngOnDestroy():void {}

  getPersonnages = (): void => {
    this.personnageService.personnageStream.subscribe(data=> {
      this.personnages = data
    },
    err => console.error(err))
  }
  addPersonnage = (personnage: string): void => {
    let objpersonnage = {name: personnage, active: true}
    this.personnageService.addPersonnage(objpersonnage).subscribe(data => {
      this.personnageService.getPersonnage();
    },
    err => console.error(err))
   }
   deletePersonnageParent = (data:number) => {
    this.personnageService.deletePersonnage(data).subscribe(data => {
    this.personnageService.getPersonnage();
    }, 
    err => console.error(err))
   }
   changeTask = (data: Personnage) => {
    this.personnageService.changeState(data).subscribe(
      data => {
        this.personnageService.getPersonnage()
      },
      err => console.error(err)
    )
  }
}

