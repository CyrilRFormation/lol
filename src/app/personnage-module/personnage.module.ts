import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonnagePageComponent } from '../pages/personnage-page/personnage-page.component';



@NgModule({
  declarations: [
    
  ],
  imports: [
    CommonModule
  ],
  exports: [
    
  ]
})
export class PersonnageModule { }
