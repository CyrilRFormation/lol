import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AjouterpersonnageComponent } from './ajouterpersonnage.component';

describe('AjouterpersonnageComponent', () => {
  let component: AjouterpersonnageComponent;
  let fixture: ComponentFixture<AjouterpersonnageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AjouterpersonnageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AjouterpersonnageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
