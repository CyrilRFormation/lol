import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-ajouterpersonnage',
  templateUrl: './ajouterpersonnage.component.html',
  styleUrls: ['./ajouterpersonnage.component.css']
})
export class AjouterpersonnageComponent implements OnInit {
  @Output() newPersonnage: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }
  addPersonnage = (data: string): void => {
    this.newPersonnage.emit(data)
  }
}
