import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PersonnagePageComponent } from './pages/personnage-page/personnage-page.component';
import { PersonnageaffichageComponent } from './components/personnageaffichage/personnageaffichage.component';
import { AjouterpersonnageComponent } from './layout/ajouterpersonnage/ajouterpersonnage.component';

@NgModule({
  declarations: [
    AppComponent,
    PersonnagePageComponent,
    PersonnageaffichageComponent,
    AjouterpersonnageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  exports: [
    PersonnagePageComponent,
    PersonnageaffichageComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
