import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Personnage } from '../model/personnage';

@Injectable({
  providedIn: 'root'
})
export class PersonnageService {

  constructor(private http: HttpClient){}

  personnageStream = new BehaviorSubject<Personnage[]>([]);

  getPersonnage = ():void => {
    this.http.get<Personnage[]>(environment.URL).subscribe(
      data => {
        this.personnageStream.next(data)
      })
  }
  
  addPersonnage = (data: Personnage): Observable<Personnage> => {
   return this.http.post<Personnage>(environment.URL, data)
  }

  deletePersonnage = (id: number):Observable<Personnage[]> => {
    return this.http.delete<Personnage[]>(`${environment.URL}/${id}`)
  }
  changeState = (data: Personnage): Observable<Personnage> => {
    return this.http.patch<Personnage>(`${environment.URL}/${data.id}`, data)
  }
}
