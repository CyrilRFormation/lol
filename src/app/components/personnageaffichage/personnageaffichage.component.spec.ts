import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonnageaffichageComponent } from './personnageaffichage.component';

describe('PersonnageaffichageComponent', () => {
  let component: PersonnageaffichageComponent;
  let fixture: ComponentFixture<PersonnageaffichageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonnageaffichageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonnageaffichageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
