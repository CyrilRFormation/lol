import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Personnage } from 'src/app/model/personnage';

@Component({
  selector: 'app-personnageaffichage',
  templateUrl: './personnageaffichage.component.html',
  styleUrls: ['./personnageaffichage.component.css'],
})
export class PersonnageaffichageComponent implements OnInit {
  
  @Input() data: Array<Personnage> = [];

  @Output() deletePersonnage:EventEmitter<number> = new EventEmitter<number>()

  @Output() changeTask:EventEmitter<Personnage> = new EventEmitter<Personnage>()
  constructor() { }

  ngOnInit(): void {
  }

  deletePersonnageEnfant = (data: number) => {
    this.deletePersonnage.emit(data)
  }

  changeState = (data: Personnage):void => {
    data.active == false ? data.active = true : data.active = false
    this.changeTask.emit(data)
}
}
